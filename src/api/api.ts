import axios from "axios";

export class CharactersService {
    static async getAll(page) {
        const response = await axios.get('https://rickandmortyapi.com/api/character/', {
            params: {
                page: page
            }
        })

        return response.data.results
    }

    static async getOne(id: number){
        const response = await axios.get(`https://rickandmortyapi.com/api/character/${id}`);

        return response.data;
    }
}

export class EpisodesService {
    static async getAll(page) {
        const response = await axios.get('https://rickandmortyapi.com/api/episode/', {
            params: {
                page: page
            }
        })

        return response.data.results
    }
}