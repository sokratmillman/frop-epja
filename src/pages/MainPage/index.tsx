import React, {FC} from 'react';

const MainPage:FC = () => {
  return (
    <div style={{height: "calc(100vh - 64px - 16px)",
                 backgroundImage: "url('https://i.pinimg.com/originals/6b/ba/73/6bba73ad75ca99ea30c9dfb3e24c55bf.jpg')",
                 backgroundSize: "cover",
                 backgroundPosition: "center"}}>
      Hello
    </div>
  )
}

export default MainPage
