import React, {FC} from 'react'
import CharactersContainer from "../../modules/CharactersContainer";

const CharactersPage:FC = () => {
  return (
    <CharactersContainer/>
  )
}

export default CharactersPage