import React, {FC} from 'react'
import { useParams } from 'react-router';
import CharacterContainer from "../../modules/CharacterContainer";

const CharacterPage:FC = ()=>{
    const { id } = useParams<{id: string}>();

    return (
        <div>
            <CharacterContainer id={id} />
        </div>
    )
}

export default CharacterPage