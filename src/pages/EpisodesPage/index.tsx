import React, {FC} from 'react'
import EpisodesContainer from "../../modules/EpisodesContainer";

const EpisodesPage:FC = ()=>{
    return (
        <div>
            <EpisodesContainer/>
        </div>
    )
}

export default EpisodesPage