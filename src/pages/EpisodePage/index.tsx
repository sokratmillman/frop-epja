import React, {FC} from 'react'
import { useParams } from 'react-router';
import EpisodeContainer from '../../modules/EpisodeContainer';

const EpisodePage:FC = ()=>{
    const { id } = useParams<{id: string}>();

    return (
        <div>
            <EpisodeContainer id={id} />
        </div>
    )
}

export default EpisodePage
