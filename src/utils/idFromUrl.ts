export function idFromUrl(url: string) : string {
    const id = url.substring(url.lastIndexOf("/")+1);
    return id;
}