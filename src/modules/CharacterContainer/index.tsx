import React , {FC, useState} from 'react';

import { Button, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { Link } from 'react-router-dom';

import {useGetCharacter} from "./hooks/useGetCharacter";
import CharacterCard from '../../components/UI/CharacterCard/CharacterCard';

type CharacterContainerProps = {
    id: string;
}

const CharacterContainer: FC<CharacterContainerProps> = ({
    id,
}) => {
    const {data, loading, error} = useGetCharacter(id);

    return (
        <>
            {loading && <p>Loading</p>}
            {!loading && data && <>
                <Box sx={{
                    margin: '40px',
                    padding: '20px',
                    display: 'flex',
                    backgroundColor: '#24325F',
                    alignItems: 'flex-start',
                    border: '2px solid white',
                }}>
                    <Box sx={{
                        padding: '2px',
                        border: '1px solid white',
                        marginRight: '20px',
                    }}>
                        <CharacterCard
                            characterName={data.character.name} 
                            characterAvatar={data.character.image}
                            characterGender={data.character.gender}
                            characterStatus={data.character.status}
                            characterLocation={data.character.location.name}/>
                    </Box>
                    <Box sx={{
                        border: '1px solid white',
                        padding: '10px',
                        marginRight: '20px',
                    }}>
                        <Typography sx={{
                            color: 'white',
                        }}>
                            <h2>Species</h2>
                            <p>{data.character.species}</p>
                            <h2>Type</h2>
                            <p>{data.character.species}</p>
                            <h2>Number of episodes:</h2> 
                            <p>{data.character.episode.length}</p>
                            <h2>Origin:</h2>
                            <p>{data.character.origin.name}</p>

                        </Typography>
                    </Box>
                    <Box sx={{
                        border: '1px solid white',
                        padding: '10px',
                        maxWidth: '40%',
                        minWidth: '30%',
                    }}>
                        <Typography sx={{
                            color: 'white',
                        }}>
                        <h2>List of episodes</h2>
                            <Box sx={{
                                display: 'flex',
                                flexWrap: 'wrap',
                            }}>
                                {data.character.episode.map(episode=>{
                                    return (
                                        <Button component={Link} sx ={{
                                            fontSize: '18px',
                                            color: 'white',
                                            textDecoration: 'underline',
                                        }}
                                            key={`episode-${episode.id}`}
                                            to={`/frop/episode/${episode.id}`}>
                                                Episode {episode.id}
                                        </Button>
                                    )
                                })}
                            </Box>
                        </Typography>
                    </Box>

                </Box>
                
            </>}
        </>
    );
};

export default CharacterContainer;