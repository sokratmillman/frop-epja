import {ApolloError, gql, useQuery} from "@apollo/client";
import {Character} from "../../../types/types";

export interface Episode {
    id: string;
    name: string;
}

interface CharacterData {
    character: {
        id: string;
        name?: string;
        status?: string;
        species?: string;
        type?: string;
        gender?: string;
        origin?: {
            name: string;
        };
        location?: {
            name: string;
        };
        image?: string;
        episode?: Episode[];
        created?: string;
    }
}

const GET_CHARACTER = gql`
    query getCharacter($id: ID!){
        character(id: $id) {
            id
            name
            status
            species
            type
            gender
            origin {
                name
            }
            location {
                name
            }
            episode {
                id
                name
            }
            image
            created
        }
    }
`

export const useGetCharacter = (id: string): { data: CharacterData; error: ApolloError; loading: boolean } => {
    const {data, error, loading} = useQuery<CharacterData>(GET_CHARACTER, {variables: {id}})
    return {data, error, loading}
}