import React , {FC} from 'react';

import { Button, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { Link } from 'react-router-dom';

import {useGetEpisode} from './hooks/useGetEpisode';
import EpisodeCard from '../../components/UI/EpisodeCard/EpisodeCard';


type EpisodeContainerProps = {
    id: string;
}

const EpisodeContainer: FC<EpisodeContainerProps> = ({
    id,
}) => {
    const {data, loading, error} = useGetEpisode(id);

    return (
        <>
            {loading && <p>Loading</p>}
            {!loading && data && <>
                <Box sx={{
                    margin: '40px',
                    padding: '20px',
                    display: 'flex',
                    backgroundColor: '#24325F',
                    alignItems: 'flex-start',
                    border: '2px solid white',
                }}>
                    <Box sx={{
                        padding: '2px',
                        border: '1px solid white',
                        marginRight: '20px',
                    }}>
                        <EpisodeCard episodeName={data.episode.name} episodeNumber={data.episode.id} />
                    </Box>
                    <Box sx={{
                        border: '1px solid white',
                        padding: '10px',
                    }}>
                        <Typography sx={{
                            color: 'white',
                        }}>
                        <h2>List of characters</h2>
                            <Box sx={{
                                display: 'flex',
                                flexWrap: 'wrap',
                            }}>
                                {data.episode.characters.map(character=>{
                                    return (
                                        <Button component={Link} sx ={{
                                            fontSize: '18px',
                                            color: 'white',
                                            textDecoration: 'underline',
                                        }}
                                            key={`character-${character.id}`}
                                            to={`/frop/character/${character.id}`}>
                                                {character.name}
                                        </Button>
                                    )
                                })}
                            </Box>
                        </Typography>
                    </Box>

                </Box>
                
            </>}
        </>
    );
};

export default EpisodeContainer;