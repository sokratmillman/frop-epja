import {ApolloError, gql, useQuery} from "@apollo/client";

export interface Character {
    id: string;
    name: string;
}
interface EpisodeData {
    episode: {
        id: string;
        name?: string;
        characters?: Character[];
    }
}

const GET_EPISODE = gql`
    query getEpisode($id: ID!){
        episode(id: $id) {
            id
            name
            characters {
                id
                name
            }
        }
    }
`

export const useGetEpisode = (id: string): { data: EpisodeData; error: ApolloError; loading: boolean } => {
    const {data, error, loading} = useQuery<EpisodeData>(GET_EPISODE, {variables: {id}})
    return {data, error, loading}
}