import {ApolloError, gql, useQuery} from "@apollo/client";
import {Episode} from "../../../types/types";

interface EpisodesData {
    episodes: {
        results: Episode[],
        info: {
            pages: number
        }
    }
}

const GET_CHARACTERS = gql`
    query getEpisodes($page: Int){
        episodes(page: $page) {
            results {
                name
                id
                episode
                air_date
                characters {
                    name
                    id
                }
            }
            info {
                pages
            }
        }
    }
`

export const useGetEpisodes=(page: Number): { data: any; error: ApolloError; loading: boolean } =>{
    const {data, error, loading} = useQuery<EpisodesData>(GET_CHARACTERS, {variables:{page}})
    return {data, error, loading}
}