import React , {useState} from 'react';

import { Pagination } from '@mui/material';

import { Box } from '@mui/system';

import {useGetEpisodes} from "./hooks/useGetEpisodes";
import CardItemList from "../../components/UI/CardItemList";
import { Entity } from '../../types/types';
import FormInput from '../../components/FormInput';

const EpisodesContainer = () => {
    const [page, setPage] = useState(1)

    const handlePaginate = (page) =>{
        setPage(page)
    }

    const [query, setQuery] = useState("")

    const {data, loading, error} = useGetEpisodes(page) 
    
    const episodesData = data?.episodes.results
    const pageCount = data?.episodes.info.pages //?

    const prependedEpisodesList = episodesData?.map(episode=>{
        return {
            linkType: Entity.Episode,
            title: episode.name,
            description: episode.episode,
            id: episode.id,
        }
    })

    const filteredEpisodesList = () => prependedEpisodesList.filter(item=>item.title.includes(query))


    return (
        <div>
            {pageCount && <Pagination count={pageCount} page={page} variant="outlined" onChange={(_, value)=>handlePaginate(value)}/>}
            <FormInput placeholder='Episode...' onQuery={(query)=>setQuery(query)}/>
            <Box mt={2}>
            {episodesData && <CardItemList items={filteredEpisodesList()}/>}
            </Box>
        </div>
    );
};

export default EpisodesContainer;