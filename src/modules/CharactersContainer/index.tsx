import React, {useCallback, useState} from 'react';

import { Pagination } from '@mui/material';
import { Box } from '@mui/system';

import {useGetCharacters} from "./hooks/useGetCharacters";
import CardItemList from "../../components/UI/CardItemList";
import FormInput from '../../components/FormInput';
import { Entity } from '../../types/types';

const CharactersContainer = () => {

    const [page, setPage] = useState(1)

    const handlePaginate = (page) =>{
        setPage(page)
    }

    const [query, setQuery] = useState("")

    const {data, loading, error} = useGetCharacters(page)
    const charactersData = data?.characters.results
    const pageCount = data?.characters.info.pages

    const prependedCharactersList = charactersData?.map(character=>{
            return {
                linkType: Entity.Character,
                title: character.name,
                description: character.location?.name,
                image: character.image,
                id: character.id
            }
        })

    const filteredCharacterList = () => prependedCharactersList.filter(item=>item.title.includes(query))

    return (
        <div>
            {pageCount && <Pagination count={pageCount} page={page} variant="outlined" onChange={(_, value)=>handlePaginate(value)}/>}
            <FormInput placeholder='Character...' onQuery={(query)=>setQuery(query)}/>
            <Box mt={2}>
            {charactersData && <CardItemList items={filteredCharacterList()}/>}
            </Box>
        </div>
    );
};

export default CharactersContainer;