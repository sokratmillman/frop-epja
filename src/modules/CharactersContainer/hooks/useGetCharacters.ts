import {ApolloError, gql, useQuery} from "@apollo/client";
import {Character} from "../../../types/types";

interface CharactersData {
    characters: {
        results: Character[]
        info: {
            pages: number
        }
    }
}

const GET_CHARACTERS = gql`
    query getCharacters($page: Int!){
        characters(page: $page) {
            results {
                id
                name
                location {
                    name
                }
                image
            }
            info {
                pages
            }
        }
    }
`

export const useGetCharacters = (page: number): { data: CharactersData; error: ApolloError; loading: boolean } => {
    const {data, error, loading} = useQuery<CharactersData>(GET_CHARACTERS, {variables: {page}})
    return {data, error, loading}
}