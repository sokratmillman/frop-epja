import React, {FC} from 'react'
import {Button, Card, CardActions, CardContent, CardMedia, Grid, Typography} from "@mui/material";
import { Link } from 'react-router-dom';

interface CharacterCardProps {
    characterName: string,
    characterStatus: string,
    characterAvatar: string,
    characterLocation: string,
    characterGender: string,
    characterLink?: string,
}

const CharacterCard: FC<CharacterCardProps> = ({
                                                   characterName,
                                                   characterStatus,
                                                   characterAvatar,
                                                   characterLocation,
                                                   characterGender,
                                                   characterLink
                                               }) => {
    return (
        <Grid item>
            <Card
                sx={{
                    maxWidth: 345, backgroundColor: '#82491E', color: 'white'
                }}
            >
                <CardMedia
                    component="img"
                    alt={characterName}
                    image={characterAvatar}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div" noWrap>
                        {characterName}
                    </Typography>
                    <Typography variant="body2" noWrap>
                        Status: {characterStatus}
                    </Typography>
                    <Typography variant="body2" noWrap>
                        Location: {characterLocation}
                    </Typography>
                    <Typography variant="body2" noWrap>
                        Gender: {characterGender}
                    </Typography>
                </CardContent>
                {characterLink && <CardActions>
                    <Button
                        component={Link}
                        to={characterLink}
                        size="small" sx={{
                        color: '#A6EEE6'
                    }}
                    >
                        Learn more
                    </Button>
                </CardActions>}
            </Card>
        </Grid>
    )
}
export default CharacterCard;
