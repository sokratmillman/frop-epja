import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import CharacterCard from './CharacterCard';
import { Entity } from '../../../types/types';

describe('CharacterCard', () => {
    test('CharacterCard renders correctly', () => {
        const tree = renderer
            .create(
                <MemoryRouter>
                    <CharacterCard
                        characterName="Kek"
                        characterStatus="ded"
                        characterAvatar=""
                        characterLocation="Earth"
                        characterGender="male"
                    />
                </MemoryRouter>
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
