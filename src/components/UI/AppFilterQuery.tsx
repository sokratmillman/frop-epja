import React, {FC} from 'react';
import {TextField} from "@mui/material";

interface filterProps {
    filter: { query:string },
    setFilter: React.Dispatch<React.SetStateAction<Object>>,
    label: string
}

const AppFilterQuery: FC<filterProps> = ({label, filter, setFilter}) => {
    const filterItems = (queryExpression) => {
        setFilter({...filter, query: queryExpression})
    }
    return (
        <TextField
            value={filter.query}
            onChange={(e) => {
                filterItems(e.target.value)
            }}
            label={label}
            color="warning"
            focused
        />
    );
}

export default AppFilterQuery;