import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import CardItemList from './index';
import { Entity } from '../../../types/types';
import { cardItemProps } from '../CardItem/cardItemProps';

describe('CardItemList', () => {
    test('CardItemList renders correctly', () => {
        const items: cardItemProps[] = [
            {
                id: "1",
                title: "title",
                linkType: Entity.Character,
                description: "description",
                height: 50,
                image: "",
            },
            {
                id: "2",
                title: "tit",
                linkType: Entity.Episode,
                description: "desc",
                height: 50,
                image: "",
            },
        ]

        const tree = renderer
            .create(
                <MemoryRouter>
                    <CardItemList
                        items={items}
                    />
                </MemoryRouter>
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
