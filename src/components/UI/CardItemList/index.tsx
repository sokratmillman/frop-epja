import React, {FC} from 'react'
import {Grid} from "@mui/material";
import CardItem from "../CardItem";
import {cardItemListProps} from "./cardItemListProps";

const CardItemList: FC<cardItemListProps> =({items}) => {
    return (
        <Grid container spacing={2}>
            {items.map(item => (
                <Grid item sm={6} xs={12} md={3} key={item.id}>
                    <CardItem title={item.title} id={item.id} description={item.description} image={item.image} linkType={item.linkType}/>
                </Grid>
            ))}
        </Grid>
    )
}

export default CardItemList