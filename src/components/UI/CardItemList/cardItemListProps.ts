import {cardItemProps} from '../CardItem/cardItemProps';

export interface cardItemListProps {
    items: cardItemProps[]
}