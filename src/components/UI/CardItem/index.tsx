import React, {FC} from 'react'
import { Link } from 'react-router-dom';

import { characterBasePath, episodeBasePath } from '../../AppRouter/routes';
import {Button, Card, CardActionArea, CardContent, CardMedia, Typography} from "@mui/material";
import {cardItemProps} from "./cardItemProps";
import { Entity } from '../../../types/types';

const CardItem: FC<cardItemProps> = (props) => {
    const prependLink = () =>{
        const baseLink = props?.linkType===Entity.Character? characterBasePath : episodeBasePath
        return  baseLink+props.id
    }
    return (
        <Card>
            <CardMedia
                component='img'
                height={240}
                image={props.image ? props.image : 'https://i.ytimg.com/vi/8bAscDw5ems/maxresdefault.jpg'}
            />
            <CardContent>
                <Typography gutterBottom variant='h5' component='div' noWrap>
                    {props.title}
                </Typography>
                <Typography variant="body2">
                    {props.description}
                </Typography>
            </CardContent>
            <CardContent>
                <Button 
                size='small'
                component={Link}
                to={prependLink()}
                 >
                     Learn More</Button>
            </CardContent>
        </Card>
    )
}

export default CardItem