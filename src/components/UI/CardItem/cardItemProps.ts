import { Entity } from '../../../types/types';

export interface cardItemProps {
    id: string,
    title: string,
    linkType: Entity,
    description: string,
    height?: number,
    image?: string,
}