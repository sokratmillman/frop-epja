import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import CardItem from './index';
import { Entity } from '../../../types/types';

describe('CardItem', () => {
    test('CardItem renders correctly', () => {
        const tree = renderer
            .create(
                <MemoryRouter>
                    <CardItem
                        id="1"
                        title="title"
                        linkType={Entity.Character}
                        description="description"
                        height={50}
                        image=""
                    />
                </MemoryRouter>
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
