import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import EpisodeCard from './EpisodeCard';

describe('EpisodeCard', () => {
    test('EpisodeCard renders correctly', () => {
        const tree = renderer
            .create(
                <MemoryRouter>
                    <EpisodeCard
                        episodeName="Kek"
                        episodeNumber="47"
                    />
                </MemoryRouter>
            )
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
