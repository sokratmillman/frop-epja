import React, {FC} from 'react'
import {Card, CardContent, CardMedia, Grid, Typography} from "@mui/material";
interface EpisodeCardProps {
    episodeName: string,
    episodeNumber: string;
}

const EpisodeCard: FC<EpisodeCardProps> = ({
    episodeName,
    episodeNumber,
}) => {
    return (
        <Grid item>
            <Card
                sx={{
                    maxWidth: 345, backgroundColor: '#82491E', color: 'white'
                }}
            >
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div" noWrap>
                        {episodeName}
                    </Typography>
                    <Typography variant="body2" noWrap>
                        Episode: {episodeNumber}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    )
}
export default EpisodeCard;
