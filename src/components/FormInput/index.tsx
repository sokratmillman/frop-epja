import React, { FC, useEffect } from 'react';
import {useForm} from 'react-hook-form';

import { Input } from '@mui/material';

interface BaseFormInterface  {
    query: string
}

interface FormProps {
    placeholder: string
    onQuery: (query: string) => void;
}

const FormInput: FC<FormProps>=  (props) => {
    const {register, handleSubmit, watch} = useForm<BaseFormInterface>({
        mode: 'onChange'
    })

    const queryInput = register("query", { required: true });
    
    useEffect(()=>{
        const subscription = watch((value, {name, type})=>{
            props.onQuery(value.query)
        })
    },[watch])

    return (
           <form
           onSubmit={(e)=>e.preventDefault()}
           >
                <Input 
                {...queryInput}
                placeholder={props.placeholder}
                />
           </form>
    );
};

export default FormInput;