import React, {FC} from 'react'
import {Route, Switch} from "react-router-dom";
import MainPage from 'src/pages/MainPage';

import routes from "./routes";

const AppRouter: FC = () => {
    return (
            <Switch>
                <Route exact path="/frop"><MainPage></MainPage></Route>
                {routes.map(route => (
                    <Route exact
                           path={route.path}
                           key={route.path}
                    >
                        {route.component}
                    </Route>
                ))}
            </Switch>
    )
}

export default AppRouter