import React from "react";

import EpisodePage from "../../pages/EpisodePage";
import CharacterPage from "../../pages/CharacterPage";
import CharactersPage from "../../pages/CharactersPage";
import EpisodesPage from "../../pages/EpisodesPage";

export default [
    {path: '/frop/episodes', name: 'Episodes', component: <EpisodesPage/>},
    {path: '/frop/characters', name: 'Characters', component: <CharactersPage/>},
    {path: '/frop/episode/:id', name: 'Episode', component: <EpisodePage/>},
    {path: '/frop/character/:id', name: 'Character', component: <CharacterPage/>},

]

export const navRoutes =[
    {path: '/frop/episodes', name: 'Episodes', component: <EpisodesPage/>},
    {path: '/frop/characters', name: 'Characters', component: <CharactersPage/>},
]

export const characterBasePath = '/frop/character/'
export const episodeBasePath = '/frop/episode/'