import React, {FC} from 'react'
import {Box, Grid} from "@mui/material";

import Header from "../Header/Header";
const MainLayout:FC = ({children})=>{
    return (
        <Grid container direction='column'>
            <Grid item>
                <Header/>
            </Grid>
            <Grid item container>
                <Grid item xs={false} sm={1}/>
                <Grid item xs={12} sm={10}>
                    <Box pt={2}>
                        {children}
                    </Box>
                </Grid>
                <Grid item xs={false} sm={1}/>
            </Grid>
        </Grid>
    )
}

export default MainLayout