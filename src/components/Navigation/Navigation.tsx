import React, {FC} from 'react'
import {Link, useLocation} from 'react-router-dom'
import {Box, MenuItem, Typography} from "@mui/material";
import {navRoutes} from "../AppRouter/routes";


const Navigation: FC = () => {
    const location = useLocation()
    return (
        <Box style={{display: 'flex'}}>
            {navRoutes.map(route => (
                <MenuItem
                    key={route.path}
                >
                    <Link
                        to={route.path}
                        style={{textDecoration: 'none', color: location.pathname === route.path ? 'white' : 'grey'}}
                    >
                        <Typography>
                            {route.name}
                        </Typography>
                    </Link>
                </MenuItem>
            ))}
        </Box>
    )
}

export default Navigation