import React, {FC} from 'react'
import {AppBar, Toolbar, Typography, Link} from "@mui/material";
import Navigation from "../Navigation/Navigation";
const Header: FC = () => {
    return (
        <AppBar position='static'>
            <Toolbar>
                <Link href={"/frop"} style={{color: "white", textDecoration: "none"}}><Typography variant='h6'>
                    Rick and Morty
                </Typography></Link>
                <Navigation/>
            </Toolbar>
        </AppBar>
    )
}

export default Header