export enum Entity {
    Character,
    Episode
}

export interface Character {
    id: string,
    name: string,
    status: string,
    image: string,
    location: { name: string, url: string },
    gender: string
}

export interface Episode {
    id: string;
    name: string;
    episode: string;
    characters: Character[];
}

export interface FullCharacter {
    id: string,
    name?: string,
    status?: string,
    species?: string,
    type?: string,
    gender?: string,
    origin?: {
        name: string,
    },
    location?: {
        name: string,
    },
    image?: string,
    episode?: string[],
    created?: string
}
