import React from 'react';
import {BrowserRouter} from "react-router-dom";

import {ApolloProvider} from "@apollo/client";
import client from "./apollo-client/apollo-client";

import AppRouter from "./components/AppRouter";
import MainLayout from "./components/MainLayout";

const App = () => {
    return (
        <BrowserRouter>
            <ApolloProvider client={client}>
                <MainLayout>
                    <AppRouter/>
                </MainLayout>
            </ApolloProvider>
        </BrowserRouter>

    )
}

export default App;

