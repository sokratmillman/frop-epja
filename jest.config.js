module.exports = {
    roots: ['<rootDir>/src'],

    coverageDirectory: '<rootDir>/reports/coverage',

    coverageReporters: [['html', { subdir: 'html' }]],

    transform: {
        '^.+\\.tsx?$': 'ts-jest',
    },
    testEnvironment: 'jsdom',

    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',

    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};
